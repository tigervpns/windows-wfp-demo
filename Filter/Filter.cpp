// Filter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <iphlpapi.h>
#include <fwpmu.h>

void EnableFilter() {
	DWORD result = ERROR_SUCCESS;
	HANDLE engine = NULL;
	FWPM_SESSION0 session;
	DWORD NICIndex;

	memset(&session, 0, sizeof(session));
	session.txnWaitTimeoutInMSec = INFINITE;
	session.flags = FWPM_SESSION_FLAG_DYNAMIC;
	result = FwpmEngineOpen0(
		NULL,
		RPC_C_AUTHN_DEFAULT,
		NULL,
		&session,
		&engine);
	if (result != ERROR_SUCCESS) {
		printf("FwpmEngineOpen0 error %08x\n", result);
		exit(result);
	}

	FwpmTransactionBegin0(engine, 0);
	printf("start transaction\n");

	////////
	// Filter should be placed here, but how?
	////////
	FWPM_FILTER0 filter;
	memset(&filter, 0, sizeof(filter));
	filter.displayData.name = L"HideMe VPN DNS Filter";
	filter.displayData.description = L"Fix DNS Leak";
	filter.flags = FWPM_FILTER_FLAG_NONE;
	filter.layerKey = FWPM_LAYER_DATAGRAM_DATA_V4;
	filter.subLayerKey = IID_NULL;
	filter.weight.type = FWP_EMPTY;
	filter.action.type = FWP_ACTION_BLOCK;
	filter.numFilterConditions = 3;
	FWPM_FILTER_CONDITION0 cond[3];
	filter.filterCondition = cond;

	cond[0].fieldKey = FWPM_CONDITION_DIRECTION;
	cond[0].matchType = FWP_MATCH_EQUAL;
	cond[0].conditionValue.type = FWP_UINT32;
	cond[0].conditionValue.uint32 = FWP_DIRECTION_OUTBOUND;

	cond[1].fieldKey = FWPM_CONDITION_INTERFACE_INDEX;
	cond[1].matchType = FWP_MATCH_EQUAL;
	GetBestInterface(0x08080808, &NICIndex);
	printf("NIC index: %d\n", NICIndex);
	cond[1].conditionValue.type = FWP_UINT32;
	cond[1].conditionValue.uint32 = NICIndex;

	cond[2].fieldKey = FWPM_CONDITION_IP_REMOTE_PORT;
	cond[2].matchType = FWP_MATCH_EQUAL;
	cond[2].conditionValue.type = FWP_UINT16;
	cond[2].conditionValue.uint16 = 53;

	// done?
	result = FwpmFilterAdd0(engine, &filter, NULL, NULL);
	if (result != ERROR_SUCCESS) {
		printf("add filter error %x\n", result);
	}
	else {
		printf("add filter done\n");
	}

	FwpmTransactionCommit0(engine);
	printf("transaction commited\n");

	printf("test DNS block now...\n");
	getchar();

	// yeah it works fine, Apr/16, Charles

	FwpmEngineClose0(engine);
	printf("fw engine closed\n");	
}

int main()
{
	EnableFilter();
    return 0;
}